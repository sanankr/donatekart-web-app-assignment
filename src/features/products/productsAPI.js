const exchangeAPIToken = "31ad28d3b2e97d7285f38628";

export const fetchCurrencyROC = async (baseCurrency) => {
  const response = await fetch(
    `https://v6.exchangerate-api.com/v6/${exchangeAPIToken}/latest/${baseCurrency}`
  );
  const data = await response.json();
  console.log("Exchange response ", data);
  return data;
};
