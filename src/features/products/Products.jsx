import { useRef } from "react";
import { useSelector } from "react-redux";
import { Container, Col } from "react-bootstrap";

import { Product } from "./Product";
import { ProductFilter } from "./ProductFilter";

import { selectProducts } from "./productsSlice";
import { useState } from "react";

export const Products = () => {
  const products = useSelector(selectProducts);
  const [filters, setFilters] = useState(null);

  const productListRef = useRef();
  const applyFilters = (filters) => {
    const {
      categBookChecked: book,
      categOutfitChecked: outfit,
      categHardwareChecked: hardware,
      categSoftwareChecked: software,
    } = filters;

    setFilters({
      book,
      outfit,
      hardware,
      software,
    });
    productListRef.current.scrollIntoView();
  };

  return (
    <Container
      style={{
        width: "100%",
        padding: 5,
        display: "flex",
        justifyContent: "flex-start",
      }}
    >
      <ProductFilter onChange={applyFilters} />
      <Col
        ref={productListRef}
        style={{
          minHeight: "100vh",
          justifyContent: "flex-start",
          display: "flex",
          flexWrap: "wrap",
          paddingTop: 125,
        }}
      >
        {products
          .filter((product) => {
            if (
              !filters ||
              (!filters.book &&
                !filters.hardware &&
                !filters.software &&
                !filters.outfit)
            ) {
              return true;
            }

            switch (product.category) {
              case "book":
                return filters.book;
              case "hardware":
                return filters.hardware;
              case "software":
                return filters.software;
              case "outfit":
                return filters.outfit;
              default:
                return false;
            }
          })
          .map((product) => (
            <Product key={product.id} {...product} />
          ))}
      </Col>
      {/* </Row> */}
    </Container>
  );
};
