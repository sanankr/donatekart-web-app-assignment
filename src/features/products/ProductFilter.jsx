import { useEffect, useState, useRef } from "react";
import { Container, Row } from "react-bootstrap";
import { RiCheckboxBlankLine, RiCheckboxFill } from "react-icons/ri";

import { CategoryIcon } from "../../components/category";

const Filter = (props) => {
  const { name, checked, onChange } = props;
  return (
    <Row
      style={{
        display: "flex",
        padding: 5,
        margin: "5px 15px",
        alignItems: "center",
        lineHeight: "0em",
        fontSize: "0.7em",
        cursor: "pointer",
      }}
      onClick={() => onChange(name, !checked)}
    >
      {/* <p > */}
      <span style={{ color: "green", marginRight: 15 }}>
        {checked ? <RiCheckboxFill /> : <RiCheckboxBlankLine />}
      </span>
      <p style={{ marginRight: 5 }}>{name}</p>
      <span>
        <CategoryIcon category={name} />
      </span>
      {/* </p> */}
    </Row>
  );
};

const FilterLabel = (props) => {
  const { name } = props;
  return (
    <Row
      style={{
        marginLeft: 5,
        fontSize: "0.8em",
        padding: "15px 0 5px",
        borderBottom: "1px solid greenyellow",
        width: "100%",
        textAlign: "left",
      }}
    >
      {name}
    </Row>
  );
};

export const ProductFilter = (props) => {
  const { onChange = () => {} } = props;
  const mounted = useRef(false);

  const [categBookChecked, setCategBookCheck] = useState(null);
  const [categOutfitChecked, setCategOutfitCheck] = useState(null);
  const [categHardwareChecked, setCategHardwareCheck] = useState(null);
  const [categSoftwareChecked, setCategSoftwareCheck] = useState(null);

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true;
      return;
    }

    onChange({
      categBookChecked,
      categOutfitChecked,
      categHardwareChecked,
      categSoftwareChecked,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    categBookChecked,
    categHardwareChecked,
    categOutfitChecked,
    categSoftwareChecked,
  ]);

  const handleFilterChange = (name, value) => {
    if (name === "Book") {
      setCategBookCheck(value);
    }
    if (name === "Hardware") {
      setCategHardwareCheck(value);
    }
    if (name === "Software") {
      setCategSoftwareCheck(value);
    }
    if (name === "Outfit") {
      setCategOutfitCheck(value);
    }
  };

  return (
    <Container
      style={{
        minWidth: 245,
        maxWidth: 245,
        height: "100vh",
        boxSizing: "border-box",
        position: "sticky",
        top: 0,
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        paddingTop: 85,
        paddingRight: 7,
        borderRight: "1px solid greenyellow",
      }}
    >
      <FilterLabel name="Category" />
      <Filter
        name="Book"
        checked={categBookChecked}
        onChange={handleFilterChange}
      />{" "}
      <Filter
        name="Hardware"
        checked={categHardwareChecked}
        onChange={handleFilterChange}
      />{" "}
      <Filter
        name="Software"
        checked={categSoftwareChecked}
        onChange={handleFilterChange}
      />{" "}
      <Filter
        name="Outfit"
        checked={categOutfitChecked}
        onChange={handleFilterChange}
      />
    </Container>
  );
};
