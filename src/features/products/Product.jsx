import { useSelector } from "react-redux";
import { Card, Row } from "react-bootstrap";
import { FaSpinner, FaRupeeSign, FaDollarSign } from "react-icons/fa";
import {
  selectCurreny,
  selectCurrencyROC,
  selectRocFetchStatus,
} from "./productsSlice";
import styles from "./Products.module.css";

import { CategoryIcon } from "../../components/category";

const CurrencyIcon = (props) => {
  const { currency } = props;

  switch (currency) {
    case "INR":
      return <FaRupeeSign />;
    case "USD":
      return <FaDollarSign />;
    default:
      return null;
  }
};

export const Product = (props) => {
  const currency = useSelector(selectCurreny);
  const conversionRate = useSelector(selectCurrencyROC);
  const rocFetchStatus = useSelector(selectRocFetchStatus);

  const { imgSrc, name, price, category } = props;

  const currPrice = Math.round(price * conversionRate * 1000) / 1000;
  return (
    <Card className={styles.product}>
      <Card>
        <img alt={name} src={imgSrc} width={275} height={320} />
      </Card>
      <p>{name}</p>
      <Row
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          lineHeight: 0,
        }}
      >
        {rocFetchStatus === "pending" ? (
          // <span style={{ marginRight: 5 }}>
          <FaSpinner className={styles.spinner} />
        ) : (
          // </span>
          <CurrencyIcon currency={currency} />
        )}
        <p style={{ marginLeft: 3 }}>{currPrice}</p>
      </Row>
      <p style={{ textAlign: "right" }}>
        <CategoryIcon category={category} />
      </p>
    </Card>
  );
};
