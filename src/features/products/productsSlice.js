import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchCurrencyROC } from "./productsAPI";
import productsTempCatalogue from "./products.catlogue.json";

const initialState = {
  catlogue: productsTempCatalogue,
  currencyROC: 1,
  currency: "INR",
  currencies: ["INR", "USD"],
  rocFetchStatus: "idle",
  // categoryFilter: [],
};

export const setCurrencyROCAsync = createAsyncThunk(
  "products/async-fetchCurrencyROC",
  async () => {
    const data = await fetchCurrencyROC("INR");
    return data;
  }
);

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    setCurrency(state, action) {
      state.currency = action.payload;
    },
    // setFilter(state, action) {
    //   state.categoryFilter.push(action.payload);
    // },
  },
  extraReducers(builder) {
    builder
      .addCase(setCurrencyROCAsync.pending, (state, action) => {
        state.rocFetchStatus = "pending";
      })
      .addCase(setCurrencyROCAsync.fulfilled, (state, action) => {
        const currency = state.currency;
        const roc = action.payload.conversion_rates[currency];
        state.currencyROC = roc;
        state.rocFetchStatus = "idle";
      });
  },
});

export const selectCurrencyROC = (state) => state.products.currencyROC;
export const selectProducts = (state) => state.products.catlogue;
export const selectCurrencies = (state) => state.products.currencies;
export const selectRocFetchStatus = (state) => state.products.rocFetchStatus;
export const selectCurreny = (state) => state.products.currency;

export const { setCurrency } = productsSlice.actions;

export default productsSlice.reducer;
