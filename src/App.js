import React from "react";
import { Header, HeaderBar } from "./components/header";
import { Products } from "./features/products/Products";

import "./App.css";
import { Footer } from "./components/footer";
function App() {
  return (
    <div className="App">
      <header className="App-header" onScroll={(e) => console.log("scrolling")}>
        <Header main="#main-catalogue" />
      </header>
      <main id="main-catalogue" className="App-main">
        <HeaderBar />
        <Products />
      </main>
      <footer className="App-footer">
        <Footer />
      </footer>
    </div>
  );
}

export default App;
