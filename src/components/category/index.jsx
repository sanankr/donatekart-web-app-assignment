import { FaBook, FaCloudDownloadAlt } from "react-icons/fa";
import { CgAdidas } from "react-icons/cg";
import { IoHardwareChip } from "react-icons/io5";

export const CategoryIcon = (props) => {
  switch (props.category.toLowerCase()) {
    case "book":
      return <FaBook />;
    case "outfit":
      return <CgAdidas />;
    case "hardware":
      return <IoHardwareChip />;
    case "software":
      return <FaCloudDownloadAlt />;
    default:
      return null;
  }
};
