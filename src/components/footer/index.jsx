import { Col, Container, Row } from "react-bootstrap";
import { FaInstagram, FaFacebook, FaTwitter } from "react-icons/fa";
import { ReactComponent as WebDevLogo } from "../../web-dev-logo.svg";

const ContactAddress = () => {
  return (
    <Container>
      <p>+91 &nbsp;&nbsp; 89898-98989</p>
      <p>
        DLF Building- Block 2, Hyderabad
        <br /> Telangana - 500032
      </p>
    </Container>
  );
};

const CopyRight = () => {
  return (
    <Container>
      <Row>
        <p>&copy; 2021 Web Dev Store</p>
      </Row>
      <Row>
        <p>Developed by Web dev group</p>
      </Row>
    </Container>
  );
};

const UsefulLinks = () => {
  return (
    <Container>
      <Row>
        <a href="mailto:support@webdev.store">Help &amp; Support</a>
      </Row>
      <Row>
        <a href="mailto:contact@webdev.store">Partner with us</a>
      </Row>
      <br />
      <Row>
        <a href="mailto:complain@webdev.store">Complain</a>
      </Row>
      <Row>
        <a href="mailto:contact@webdev.store">Contact</a>
      </Row>
    </Container>
  );
};

const SocialMediaLinks = () => {
  return (
    <Container
      style={{
        display: "flex",
        width: 85,
        margin: "auto",
        justifyContent: "space-evenly",
        alignSelf: "flex-start",
        color: "black",
        fontSize: "1.5em",
      }}
    >
      <a href="https://twitter.com" target="_blank" rel="noreferrer">
        <FaTwitter />
      </a>
      <a href="https://instagram.com" target="_blank" rel="noreferrer">
        <FaInstagram />
      </a>
      <a href="https://facebook.com" target="_blank" rel="noreferrer">
        <FaFacebook />
      </a>
    </Container>
  );
};
export const Footer = () => {
  return (
    <Container
      style={{
        minHeight: "25vh",
        backgroundColor: "#5bcda3",
        paddingTop: 20,
        fontSize: "0.7em",
      }}
    >
      <Row
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          padding: 12,
        }}
      >
        <Col>
          <WebDevLogo />
          <ContactAddress />
        </Col>
        <Col>
          <UsefulLinks />
        </Col>
        <Col>
          <SocialMediaLinks />
          <CopyRight />
        </Col>
      </Row>
    </Container>
  );
};
