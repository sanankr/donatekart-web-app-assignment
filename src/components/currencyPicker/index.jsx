import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  selectCurrencies,
  setCurrency,
  setCurrencyROCAsync,
} from "../../features/products/productsSlice";

export const CurrencyPicker = () => {
  const defaultCurrency = "INR";
  const currencies = useSelector(selectCurrencies);

  const [selectedCurrency, setSelectedCurrency] = useState(null);

  const dispatch = useDispatch();

  const handleCurrencyChange = (e) => {
    const currency = e.target.value;

    setSelectedCurrency(e.target.value);

    dispatch(setCurrency(currency));
    dispatch(setCurrencyROCAsync());
  };

  return (
    <select
      style={{
        outline: "none",
        border: "3px solid #5bcda3",
        fontSize: "0.6em",
        borderRadius: 2,
        cursor: "pointer",
      }}
      value={selectedCurrency || defaultCurrency}
      onChange={handleCurrencyChange}
    >
      {currencies.map((currency) => (
        <option key={currency} value={currency}>
          {currency}
        </option>
      ))}
    </select>
  );
};
