import { useState, useEffect, useRef } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { CurrencyPicker } from "../currencyPicker";
import { ReactComponent as WebDevLogo } from "../../web-dev-logo.svg";

export const Header = (props) => {
  return (
    <Container style={{ fontSize: "0.9em", lineHeight: "0.2em" }}>
      <Row>
        <Col md={6}>
          <a href={props.main ?? "#"}>
            <WebDevLogo />
          </a>
          <p style={{ fontSize: "0.45em" }}>
            <p>
              A marketplace for developers - browse the devlopment companion
            </p>
          </p>
          <p></p>
        </Col>

        <Col md={6}></Col>
      </Row>
    </Container>
  );
};

export const HeaderBar = () => {
  const [showHeaderBar, setShowHeaderBar] = useState(false);
  const barRef = useRef();
  useEffect(() => {
    const innerHeight = window.innerHeight;
    document.body.onscroll = () => {
      if (document.documentElement.scrollTop > innerHeight - 10) {
        setShowHeaderBar(true);
      } else {
        setShowHeaderBar(false);
      }
    };
  }, []);

  return (
    <Container
      ref={barRef}
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        position: "sticky",
        top: 0,
        boxShadow: "0 0 5px 0 green",
        backgroundColor: "white",
        visibility: showHeaderBar ? "visible" : "hidden",
      }}
    >
      <Col xs={2} style={{ padding: 12 }}>
        <WebDevLogo />
      </Col>
      <Col xs={2} style={{ padding: 12 }}>
        <CurrencyPicker />
      </Col>
    </Container>
  );
};
