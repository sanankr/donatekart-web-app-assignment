# Donatekart WEB APP assignment

**Assignment**
>
> Create a single page application to show products and cost in the chosen currency.
>
>1. Create a git repo in Github.com or any other platform and push all your code to it.
>2. Use HTML5, bootstrap, AngularJS or any other JavaScript framework you prefer.
>3. Create a products page to display list of products. Each product has image, name and price in Indian Rupees.
Just display any three products, get the images from google, set your price (using json object).
>
>4. Create Currency dropdown with USD and INR.
>5. If we change the currency to USD, then the price of the products should be displayed in us dollars by using the exchange rates obtained from the api given below
>      - Go to https://www.exchangerate-api.com/ and get a Free API key by providing your email. Then use the url https://v6.exchangerate-api.com/v6/{your_apikey}/latest/INR to get the live exchange rates.

**Web APP**
>
>[Web Dev Store](https://sanankr.gitlab.io/donatekart-web-app-assignment)

**Remark:**
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.
